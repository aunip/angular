import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './components/app.component';
import { ListComponent, InfoComponent, AddComponent } from './components/containers';
import { BlockComponent, HyperLinkComponent, RadioComponent, RowComponent, TextFieldComponent } from './components/layers';
import { PizzaService } from './services/pizza.service';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    AddComponent,
    InfoComponent,
    BlockComponent,
    HyperLinkComponent,
    RadioComponent,
    RowComponent,
    TextFieldComponent
  ],
  imports: [BrowserModule],
  providers: [PizzaService],
  bootstrap: [AppComponent]
})
export class AppModule {}
