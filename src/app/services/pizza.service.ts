import { Injectable } from '@angular/core';
import { generateId } from '../utils';
import pizzas from '../../pizzas.json';

export interface Pizza {
  id?: string;
  label: string;
  items: string[];
  price: number;
}

type PizzaOrNull = Pizza | null;

@Injectable({
  providedIn: 'root'
})
export class PizzaService {
  pizza: PizzaOrNull = null;
  pizzas: Pizza[] = [];

  constructor() {
    this.pizzas = pizzas.map((pizza: Pizza) => {
      pizza.id = generateId();
      return pizza;
    });
  }

  readPizza(pizza: PizzaOrNull) {
    this.pizza = pizza;
  }

  addToPizzas(newPizza: Pizza) {
    this.pizzas = [...this.pizzas, { id: generateId(), ...newPizza }];
  }

  upToPizzas(currentPizza: Pizza) {
    this.pizzas = this.pizzas.map(p => (p.id === currentPizza.id ? currentPizza : p));
  }

  delToPizzas(id: string) {
    this.pizzas = this.pizzas.filter(p => p.id !== id);
  }
}
