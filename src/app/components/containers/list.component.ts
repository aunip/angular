import { Component, OnInit } from '@angular/core';
import { PizzaService, Pizza } from '../../services/pizza.service';
import { lo, scrollToListView } from '../../utils';

@Component({
  selector: 'list',
  templateUrl: './list.component.html'
})
export class ListComponent implements OnInit {
  filter = '';

  constructor(private pizzaService: PizzaService) {}

  ngOnInit() {
    setTimeout(() => {
      scrollToListView();
    }, 1000);
  }

  getAllPizzas(): Pizza[] {
    return this.pizzaService.pizzas;
  }

  getFilteredPizzas(): Pizza[] {
    return this.pizzaService.pizzas.filter(({ label }) => lo(label).includes(lo(this.filter)));
  }

  byKey(key: string) {
    return (a, b) => (a[key] > b[key] ? 1 : a[key] < b[key] ? -1 : 0);
  }

  setPizza(pizza: Pizza) {
    this.pizzaService.readPizza(pizza);
  }

  setFilter(event) {
    this.filter = event.target.value;
  }
}
