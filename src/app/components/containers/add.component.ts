import { Component } from '@angular/core';
import { PizzaService } from '../../services/pizza.service';
import { capitalize } from '../../utils';

@Component({
  selector: 'add',
  templateUrl: './add.component.html'
})
export class AddComponent {
  label = '';
  items = [''];
  price = 0;

  constructor(private pizzaService: PizzaService) {}

  setLabel(event) {
    this.label = event.target.value;
  }

  setPrice(event) {
    this.price = parseFloat(event.target.value);
  }

  isValid(): boolean {
    const allItems: string[] = this.items.filter(item => item.length > 0);

    return this.label.length > 0 && allItems.length > 0 && this.price > 0;
  }

  addItem(index: number) {
    return event => {
      const value = event.target.value;

      const newItems: string[] = this.items.map((item, idx) => (idx === index ? value : item));

      if (value.length > 0) {
        if (this.items[index + 1] === undefined) {
          this.items = [...newItems, ''];
        } else {
          this.items = newItems;
        }
      } else {
        this.items = newItems.filter((_, idx) => idx !== index + 1);
      }
    };
  }

  resetPizza() {
    this.pizzaService.readPizza(null);
  }

  addNewPizza() {
    if (this.isValid()) {
      this.pizzaService.addToPizzas({
        label: capitalize(this.label),
        items: this.items.filter(item => item.length > 0),
        price: this.price
      });

      this.resetPizza();
    }
  }

  trackByFunc(index: number): number {
    return index;
  }
}
