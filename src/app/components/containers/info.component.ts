import { Component } from '@angular/core';
import { PizzaService } from '../../services/pizza.service';
import { capitalize } from '../../utils';

@Component({
  selector: 'info',
  templateUrl: './info.component.html'
})
export class InfoComponent {
  label: string;
  items: string[];
  price: number;
  takeOff: string[] = [];
  locked = true;

  constructor(private pizzaService: PizzaService) {
    const pizza = this.pizzaService.pizza;

    this.label = pizza.label;
    this.items = pizza.items;
    this.price = pizza.price;
  }

  setLabel(event) {
    this.label = event.target.value;
  }

  setItem(index: number) {
    return event => {
      this.items[index] = event.target.value;
    };
  }

  setPrice(event) {
    this.price = parseFloat(event.target.value);
  }

  handleTakeOff(item: string) {
    if (this.takeOff.find(i => i === item)) {
      this.takeOff = this.takeOff.filter(i => i !== item);
    } else {
      this.takeOff = [...this.takeOff, item];
    }
  }

  resetPizza() {
    this.pizzaService.readPizza(null);
  }

  removePizza() {
    const pizza = this.pizzaService.pizza;

    this.pizzaService.delToPizzas(pizza.id);
    this.resetPizza();
  }

  updatePizza() {
    if (!this.locked) {
      const pizza = this.pizzaService.pizza;

      this.pizzaService.upToPizzas({
        id: pizza.id,
        label: capitalize(this.label),
        items: this.items,
        price: this.price
      });
    }

    this.locked = !this.locked;
  }

  trackByFunc(index: number): number {
    return index;
  }
}
