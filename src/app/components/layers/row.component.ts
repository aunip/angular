import { Component } from '@angular/core';

@Component({
  selector: 'row',
  template: `
    <div class="row">
      <div class="icon">
        <ng-content select="[iconCell]"></ng-content>
      </div>
      <div class="line">
        <div class="left">
          <ng-content select="[leftCell]"></ng-content>
        </div>
        <div class="right">
          <ng-content select="[rightCell]"></ng-content>
        </div>
      </div>
    </div>
  `
})
export class RowComponent {}
