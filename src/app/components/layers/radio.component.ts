import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'radio',
  template: `
    <svg fill="none" width="40" height="25" (click)="onClick()" style="cursor: pointer;">
      <circle *ngIf="checked" cx="20" cy="12.5" r="5" [attr.fill]="color" />
      <circle cx="20" cy="12.5" r="10" [attr.stroke]="color" stroke-width="3" />
    </svg>
  `
})
export class RadioComponent {
  @Input() color = '#212121';
  @Output() handleClick = new EventEmitter();

  checked = true;

  onClick() {
    if (this.handleClick) {
      this.handleClick.emit();
    }

    this.checked = !this.checked;
  }
}
