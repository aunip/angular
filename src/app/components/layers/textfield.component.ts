import { Component, Input, Output, EventEmitter } from '@angular/core';

type StringOrNumber = string | number;

@Component({
  selector: 'text-field',
  template: `
    <input
      *ngIf="editable; else text"
      [type]="type"
      [placeholder]="placeholder"
      [value]="value"
      (input)="onInput($event)"
      [style.font-size]="size + 'px'"
    />
    <ng-template #text>
      <p [style.font-size]="size + 'px'">
        <ng-content></ng-content>
      </p>
    </ng-template>
  `
})
export class TextFieldComponent {
  @Input() type = 'text';
  @Input() placeholder: string;
  @Input() value: StringOrNumber;
  @Input() size = 16;
  @Input() editable = false;
  @Output() handleInput = new EventEmitter();

  onInput(event: Event) {
    this.handleInput.emit(event);
  }
}
