import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'hyper-link',
  template: `
    <a [href]="to" (click)="onClick()" [style.font-size]="size + 'px'">
      <ng-content></ng-content>
    </a>
  `
})
export class HyperLinkComponent {
  @Input() to = '#';
  @Input() size = 16;
  @Output() handleClick = new EventEmitter();

  checked = true;

  onClick() {
    this.handleClick.emit();
  }
}
