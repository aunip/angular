import { Component } from '@angular/core';
import { PizzaService, Pizza } from '../services/pizza.service';

@Component({
  selector: 'app',
  template: `
    <list *ngIf="!getPizza()"></list>
    <info *ngIf="getPizza() && getPizza().id"></info>
    <add *ngIf="getPizza() && !getPizza().id"></add>
  `
})
export class AppComponent {
  constructor(private pizzaService: PizzaService) {}

  getPizza(): Pizza {
    return this.pizzaService.pizza;
  }

  getAllPizzas(): Pizza[] {
    return this.pizzaService.pizzas;
  }
}
