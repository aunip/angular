/**
 * Generate ID
 */
export const generateId = (): string => {
  return Math.random()
    .toString(36)
    .substring(2);
};

/**
 * toUpperCase() ShortCut
 *
 * @param text Text
 * @returns Formatted 'TEXT'
 */
export const up = (text: string): string => text.toUpperCase();

/**
 * toLowerCase() ShortCut
 *
 * @param text Text
 * @returns Formatted 'text'
 */
export const lo = (text: string): string => text.toLowerCase();

/**
 * Capitalize
 *
 * @param text Text
 * @returns Formatted 'Text'
 */
export const capitalize = (text: string): string => up(text.substring(0, 1)) + lo(text.substring(1, text.length));

/**
 * Scroll To ListView
 */
export const scrollToListView = () => {
  const listView = document.querySelector('.listview');
  listView.scrollTo({
    top: 40,
    left: 0,
    behavior: 'smooth'
  });
};
